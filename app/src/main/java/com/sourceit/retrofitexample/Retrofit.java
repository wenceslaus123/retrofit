package com.sourceit.retrofitexample;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by wenceslaus on 27.01.18.
 */

public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/v1/all")
        void getCountries(Callback<List<Country>> callback);

        @GET("/v2/name/{name}")
        void getCountriesByName(@Path(value = "name") String name, Callback<List<Country>> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(Callback<List<Country>> callback) {
        apiInterface.getCountries(callback);
    }

    public static void getCountriesByName(Callback<List<Country>> callback) {
        apiInterface.getCountriesByName("uk", callback);
    }

}

