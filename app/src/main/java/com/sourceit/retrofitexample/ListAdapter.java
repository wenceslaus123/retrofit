package com.sourceit.retrofitexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by wenceslaus on 27.01.18.
 */

public class ListAdapter extends BaseAdapter {

    List<Country> list;
    Context context;

    public ListAdapter(Context context, List<Country> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Country getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View root = convertView;

        if (root == null) {
            root = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
            viewHolder = new ViewHolder(root);
            root.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) root.getTag();
        }

        Country country = getItem(position);
        viewHolder.name.setText(country.name);

        return root;
    }

    static class ViewHolder {
        TextView name;

        ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.name);
        }
    }
}
